package devoxxinfoapp;

import com.sun.glass.ui.Screen;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.effect.PerspectiveTransform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author Jasper Potts
 */
public class DevoxxInfoApp extends Application {
    static {
        System.setProperty("http.proxyHost", "www-proxy.us.oracle.com");
        System.setProperty("http.proxyPort", "80");
    }
    
    private static DateFormat DEBUG_DATE_FORMAT = DateFormat.getDateTimeInstance();
    private static DateFormat TIME24_FORMAT = new SimpleDateFormat("HH:mm");
    private ImageView background = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/Devoxx-wide.jpg").toExternalForm()));
    private ImageView javaPowered = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/java-powered.png").toExternalForm()));
    private DoubleProperty bgPos = new SimpleDoubleProperty(0) {
        @Override protected void invalidated() {
            background.setViewport(new Rectangle2D(get()*(4096-1920), 0, 1920, 1080));
        }
    };
    private Date currentTimeOverride = null;
    private Text currentTimeOverrideText = null;
    private Text roomNumber;
    private Text desc;
    private Presentation currentPresentation = null;
    private List<Presentation> presentations;
    private Font roomNumberFont;
    private Font speakerFont;
    private Font titleFont;
    private Font smallTitleFont;
    private Font smallTrackFont;
    private Font smallTimesFont;
    private Font bodyFont;
    private LargeSign largeSign;
    private SmallSign smallSign1;
    private SmallSign smallSign2;
    private Timeline timeline;
    
    @Override public void start(Stage primaryStage) {
        String room = getParameters().getRaw().isEmpty() ? null : getParameters().getRaw().get(0);
        if (room == null || room.isEmpty()) room = "Room 8";
        
        final boolean testMode = getParameters().getRaw().size() >= 2 && getParameters().getRaw().get(1).equals("test");
        System.out.println("=========================================================");
        System.out.println("== DEVOXX DISPLAY APP for room ["+room+"]");
        if (testMode) System.out.println(" !!!! TEST MODE !!!!");
        System.out.println("=========================================================");
        
        // LOAD FONTS
        System.out.println("Loading Fonts...");
        Font bold = Font.loadFont(DevoxxInfoApp.class.getResource("BenchNine/BenchNine-Bold.ttf").toExternalForm(), 10);
        Font light = Font.loadFont(DevoxxInfoApp.class.getResource("BenchNine/BenchNine-Light.ttf").toExternalForm(), 10);
        Font regular = Font.loadFont(DevoxxInfoApp.class.getResource("BenchNine/BenchNine-Regular.ttf").toExternalForm(), 10);
        Font owbold = Font.loadFont(DevoxxInfoApp.class.getResource("Oswald/Oswald-Bold.ttf").toExternalForm(), 10);
        
        roomNumberFont = Font.font(owbold.getName(), 500);
        titleFont = Font.font(light.getName(), 60);
        smallTitleFont = Font.font(regular.getName(), 35);
        smallTimesFont = Font.font(bold.getName(), 50);
        smallTrackFont = Font.font(regular.getName(), 24);
        bodyFont = Font.font(regular.getName(), 30);
        speakerFont = Font.font(regular.getName(), 35);
        // FETCH DATA
        presentations = new DataFetcher(room).getData();
        
        // SETUP UI
        largeSign = new LargeSign();
        smallSign1 = new SmallSign();
        smallSign2 = new SmallSign();
        background.setViewport(new Rectangle2D(0, 0, 1920, 1080));
        smallSign1.setTranslateY(20);
        smallSign2.setTranslateY(25);
        
        System.out.println("ROOM NUMBER = ["+room.split("\\s+")[1]+"]");
        roomNumber = new Text(room.split("\\s+")[1]);
        roomNumber.setFill(Color.rgb(0,0,0,0.4));
        roomNumber.setFont(roomNumberFont);
        roomNumber.setTranslateX(1920-30-roomNumber.getLayoutBounds().getWidth());
        roomNumber.setTranslateY(1080-30);
        roomNumber.setCache(true);
        
        javaPowered.setTranslateX(30);
        javaPowered.setTranslateY(1080-236-30);
        
//        desc = new Text("Powered by \u20AC30 Raspberry Pi");
//        roomNumber.setFill(Color.rgb(0,0,0,0.4));
//        roomNumber.setFont(titleFont);
//        roomNumber.setTranslateX((1920-roomNumber.getLayoutBounds().getWidth())/2);
//        roomNumber.setTranslateY(1080-30);
        
        final Group root = new Group();
        root.getChildren().addAll(background,javaPowered, roomNumber, smallSign1, smallSign2, largeSign);
        
        final double screenWidth = Screen.getMainScreen().getWidth();
        final double screenHeight = Screen.getMainScreen().getHeight();
        
        Scene scene;
        if (screenWidth < 1500) {
            root.getTransforms().add(new Scale(screenWidth/1920, screenHeight/1080));
            scene = new Scene(root, screenWidth, screenHeight);
        } else {
            scene = new Scene(root, 1920, 1080);
        }
        
        scene.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.PRIMARY) {
                    if (currentTimeOverride == null) {
                        currentTimeOverride = presentations.get(0).fromTime;
                        currentTimeOverrideText = new Text();
                        currentTimeOverrideText.setFont(Font.font("System", 30));
                        currentTimeOverrideText.setFill(Color.RED);
                        currentTimeOverrideText.setTextOrigin(VPos.TOP);
                        currentTimeOverrideText.setX(10);
                        currentTimeOverrideText.setY(10);
                        currentTimeOverrideText.setText(DEBUG_DATE_FORMAT.format(currentTimeOverride));
                        root.getChildren().add(currentTimeOverrideText);
                    } else {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(currentTimeOverride);
                        calendar.add(Calendar.MINUTE,30);
                        currentTimeOverride = calendar.getTime();
                        currentTimeOverrideText.setText(DEBUG_DATE_FORMAT.format(currentTimeOverride));
                    }
                    update();
                } else if (event.getButton() == MouseButton.SECONDARY) {
                    goNext();
                } else if (event.getButton() == MouseButton.MIDDLE) {
                    currentTimeOverride = null;
                    root.getChildren().remove(currentTimeOverrideText);
                    currentTimeOverrideText = null;
                    update();
                }
            }
        });
        
        timeline = new Timeline(
            new KeyFrame(Duration.ZERO, 
                new KeyValue(bgPos, 0, Interpolator.EASE_BOTH),
                new KeyValue(largeSign.translateXProperty(), -1997, Interpolator.LINEAR),
                new KeyValue(largeSign.translateYProperty(), 1080-694),
                new KeyValue(largeSign.engineOpacityProperty(), 1),
                new KeyValue(smallSign1.translateXProperty(), 1920, Interpolator.DISCRETE),
                new KeyValue(smallSign2.translateXProperty(), 1920, Interpolator.DISCRETE)
            ),
            new KeyFrame(Duration.minutes(0.14), 
                new KeyValue(largeSign.engineOpacityProperty(), 1)
            ),
            new KeyFrame(Duration.minutes(0.15), 
                new KeyValue(largeSign.translateXProperty(), 0, Interpolator.SPLINE(0.1592, 0.4183, 0.5079, 0.9497)),
                new KeyValue(largeSign.engineOpacityProperty(), 0),
                new KeyValue(smallSign1.translateXProperty(), 1920)
            ),
            new KeyFrame(Duration.minutes(0.35),
                new KeyValue(smallSign2.translateXProperty(), 1920)
            ),
            new KeyFrame(Duration.minutes(0.7),
                new KeyValue(smallSign1.translateXProperty(), -797)
            ),
            new KeyFrame(Duration.minutes(0.71),
                new KeyValue(smallSign1.translateXProperty(), 1920, Interpolator.DISCRETE)
            ),
            new KeyFrame(Duration.minutes(0.90),
                new KeyValue(smallSign2.translateXProperty(), -797)
            ),
            new KeyFrame(Duration.minutes(1.0),
                new KeyValue(smallSign2.translateXProperty(), 1920, Interpolator.DISCRETE)
            ),
            new KeyFrame(Duration.minutes(1.60),
                new KeyValue(smallSign1.translateXProperty(), -797)
            ),
            new KeyFrame(Duration.minutes(1.9),
                new KeyValue(smallSign2.translateXProperty(), -797)
            ),
                
            new KeyFrame(Duration.minutes(1.89),
                new KeyValue(largeSign.engineOpacityProperty(), 0)
            ),
            new KeyFrame(Duration.minutes(1.891),
                new KeyValue(largeSign.engineOpacityProperty(), 1)
            ),
            new KeyFrame(Duration.minutes(1.893),
                new KeyValue(largeSign.engineOpacityProperty(), 0)
            ),
            new KeyFrame(Duration.minutes(1.896),
                new KeyValue(largeSign.engineOpacityProperty(), 1)
            ),
            new KeyFrame(Duration.minutes(1.898),
                new KeyValue(largeSign.engineOpacityProperty(), 0)
            ),
            new KeyFrame(Duration.minutes(1.9), 
                new KeyValue(largeSign.translateXProperty(), 400, Interpolator.LINEAR),
                new KeyValue(largeSign.translateYProperty(), 1080-694-180, Interpolator.LINEAR),
                new KeyValue(largeSign.engineOpacityProperty(), 1)
            ),
            new KeyFrame(Duration.minutes(2), 
                new KeyValue(bgPos, 1, Interpolator.EASE_BOTH),
                new KeyValue(largeSign.translateXProperty(), 1920, Interpolator.SPLINE(0.2974, 0.0054, 0.8197, 0.0305))//,
//                new KeyValue(largeSign.translateYProperty(), 1080-694-250, Interpolator.LINEAR)
            ),
            new KeyFrame(Duration.minutes(2.12), 
                new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent t) {
                        if (testMode) {
                            goNext();
                        } else {
                            update();
                        }
                    }
                },
                new KeyValue(bgPos, 0, Interpolator.EASE_BOTH),
                new KeyValue(largeSign.translateYProperty(), 1080-694-220, Interpolator.LINEAR)
            )
        );
//        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setCycleCount(1);
        
        primaryStage.setScene(scene);
        primaryStage.show();
        
        update();
    }
    
    private void update() {
        Date now = (currentTimeOverride != null) ? currentTimeOverride : new Date();
        System.out.println("NOW = "+now+"  - currentTimeOverride = "+currentTimeOverride);
        List<Presentation> newPresentations = new ArrayList<>();
        for (Presentation presentation: presentations) {
            if (now.before(presentation.toTime)) {
                newPresentations.add(presentation);
            }
            if (newPresentations.size() >= 3) break;
        }
        Presentation first = newPresentations.size() >= 1 ? newPresentations.get(0) : null;
        Presentation second = newPresentations.size() >= 2 ? newPresentations.get(1): null;
        Presentation third = newPresentations.size() >= 3 ? newPresentations.get(2): null;
        System.out.println("UPDATE @ ("+DEBUG_DATE_FORMAT.format(now)+")");
        if (currentPresentation != first) {
            currentPresentation = first;
            System.out.println("         "+first);
            System.out.println("         "+second);
            System.out.println("         "+third);
            largeSign.setPresentation(first);
            smallSign1.setPresentation(second);
            smallSign2.setPresentation(third);
        }
        
        timeline.playFromStart();
    }
    
    private void goNext() {
        int index = presentations.indexOf(currentPresentation);
        if (index == presentations.size()-2) index = -1;
        Presentation p = presentations.get(index+1);
        currentTimeOverride = p.fromTime;
        update();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    private class LargeSign extends Region {
        private ImageView largeSignLeft = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/large-sign-left.png").toExternalForm()));
        private ImageView largeSignRight = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/large-sign-right.png").toExternalForm()));
        private ImageView shipEngOff = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/large-ship-engines-off.png").toExternalForm()));
        private ImageView shipEngOn = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/large-ship-engines-on.png").toExternalForm()));
        private ImageView shipEngOnBack = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/large-ship-engines-on-back.png").toExternalForm()));
        private DoubleProperty engineOpacity = new SimpleDoubleProperty(0) {
            @Override protected void invalidated() {
                final double val = get();
                shipEngOn.setOpacity(val);
                shipEngOnBack.setOpacity(val);
            }
        };
        private Presentation presentation;
        private VBox textContent = new VBox(0);
        private final SpeakerBar speakerBar = new SpeakerBar();
        private final Pane contentPane = new Pane() {
            { getChildren().addAll(textContent,speakerBar); }
            
            @Override protected void layoutChildren() {
                int width = (int)getWidth();
                int height = (int)getHeight();
                int speakerBarWidth = (int)(speakerBar.prefWidth(height)+0.5d);
                speakerBar.setLayoutX(width-speakerBarWidth);
                speakerBar.resize(speakerBarWidth, height);
                final int textWidth = width - speakerBarWidth - 20;
                textContent.resize(textWidth,height);
                // update wrapping
                title.setWrappingWidth(textWidth);
                body.setWrappingWidth(textWidth);
            }
        };
        private final Text title = new Text();
        private final Text details = new Text();
//        private final Text body = new Text();
        private final TwoColumnTextNode body = new TwoColumnTextNode(1300);
        
        public LargeSign() {
            largeSignLeft.setTranslateY(23);
            largeSignRight.setTranslateX(1024);
            shipEngOff.setTranslateX(1385);
            shipEngOff.setTranslateY(29);
            shipEngOn.setTranslateX(1215);
            shipEngOn.setTranslateY(29+174);
            shipEngOnBack.setTranslateX(1215);
            shipEngOnBack.setTranslateY(197);
            
            title.setFont(titleFont);
            title.setWrappingWidth(1300);
            title.setFill(Color.WHITE);
            textContent.getChildren().add(title);
            
            details.setFont(smallTrackFont);
            details.setWrappingWidth(1300);
            details.setFill(Color.WHITE);
            textContent.getChildren().add(details);
            
            body.setFont(bodyFont);
            body.setWrappingWidth(1300);
            body.setFill(Color.WHITE);
            textContent.getChildren().add(body);
            
//            textContent.setStyle("-fx-background-color:cyan;");
//            contentPane.setStyle("-fx-background-color:yellow;");
            
            contentPane.setEffect(new PerspectiveTransform(
                    63,143,
                    63+1300+15,47,
                    63+1300-7,47+425,
                    63-5,143+500));
            contentPane.setCache(true);
            
            getChildren().addAll(shipEngOnBack,shipEngOff,largeSignLeft, largeSignRight,contentPane,shipEngOn);
        }
        
        @Override protected void layoutChildren() {
            contentPane.resize(1300, 500);
        }

        public DoubleProperty engineOpacityProperty() {
            return engineOpacity;
        }

        public void setPresentation(Presentation presentation) {
            this.presentation = presentation;
            title.setText((presentation==null) ? null : TIME24_FORMAT.format(presentation.fromTime)+" - "+TIME24_FORMAT.format(presentation.toTime)+" | "+presentation.title);
            details.setText((presentation==null) ? null : "["+presentation.track+"] ["+presentation.level+"]");
            body.setText(presentation.summary);
            speakerBar.setSpeakers((presentation==null) ? new Speaker[]{} : presentation.speakers);
        }
    }
    
    private class SmallSign extends Region {
        private ImageView ship = new ImageView(new Image(DevoxxInfoApp.class.getResource("images/little-ship.png").toExternalForm()));
        private VBox textContent = new VBox(0);
        private Presentation presentation;
        private final Text times = new Text();
        private final Text track = new Text();
        private final Text title = new Text();
        private final SpeakerBar speakerBar = new SpeakerBar();
        
        public SmallSign() {
            times.setFont(smallTimesFont);
            times.setFill(Color.WHITE);
            track.setFont(smallTrackFont);
            track.setFill(Color.WHITE);
            title.setFont(smallTitleFont);
            title.setFill(Color.WHITE);
            textContent.getChildren().addAll(times,title,track);
            
            speakerBar.setCache(true);
            textContent.setCache(true);
            
            getChildren().addAll(ship,textContent,speakerBar);
        }

        @Override protected void layoutChildren() {
            final int left = 301;
            final int top = 18;
            int width = 477;
            int height = 237;
            int speakerBarWidth = (int)(speakerBar.prefWidth(height)+0.5d);
            speakerBar.setLayoutX(left+width-speakerBarWidth);
            speakerBar.setLayoutY(top);
            speakerBar.resize(speakerBarWidth, height);
            width -= speakerBarWidth + 20;
            textContent.setLayoutX(left);
            textContent.setLayoutY(top-4);
            textContent.resize(width,height);
            textContent.setClip(new Rectangle(width,height+4));
            
            title.setWrappingWidth(width);
        }

        public void setPresentation(Presentation presentation) {
            this.presentation = presentation;
            times.setText((presentation==null) ? null : TIME24_FORMAT.format(presentation.fromTime)+" - "+TIME24_FORMAT.format(presentation.toTime));
            track.setText((presentation==null) ? null : "["+presentation.track+"]");
            title.setText((presentation==null) ? null : presentation.title);
            speakerBar.setSpeakers((presentation==null) ? new Speaker[]{} : presentation.speakers);
        }
    }
    
    private class SpeakerBar extends Region {
        private final VBox box = new VBox(5);
        private final ObservableList<Node> boxChildren = box.getChildren();
        
        public SpeakerBar() {
            box.setAlignment(Pos.CENTER);
            setStyle("-fx-border-color: transparent transparent transparent rgb(255,255,255,0.5); -fx-border-width: 2; -fx-border-insets: 10 0 10 -10;");
            getChildren().add(box);
        }
        
        public void setSpeakers(Speaker[] speakers) {
            boxChildren.clear();
            for (Speaker speaker: speakers) {
                Image image = speaker.image;
                if (image == null) {
                    try {
                        image = new Image(speaker.imageUrl);
                    } catch (Exception e) {
                        System.err.println("Error loading: ["+speaker.imageUrl+"]");
                        e.printStackTrace();
                        image = null;
                    }
                    speaker.image = image;
                }
                ImageView img = new ImageView(image);
                boxChildren.add(img);
                Text text = new Text(speaker.fullName);
                text.setFill(Color.WHITE);
                text.setFont(speakerFont);
                boxChildren.add(text);
            }
        }

        @Override protected void layoutChildren() {
            final double height = getHeight();
            final double scale = calcScale(height);
            if (scale == 1) {
                box.getTransforms().clear();
                box.resize(getWidth(), height);
            } else {
                box.getTransforms().setAll(new Scale(scale,scale,0,0));
                box.resize(box.prefWidth(-1), box.prefHeight(-1));
            }
        }
        
        private double calcScale(double height) {
            final double boxHeight = box.prefHeight(-1);
            if (boxHeight > height) {
                return height/boxHeight;
            } else {
                return 1;
            }
        }

        @Override protected double computePrefWidth(double d) {
            final double boxWidth = box.prefWidth(-1); 
            if(d == -1) {
                return boxWidth;
            } else {
                final double scale = calcScale(d);
                return boxWidth * scale;
            }
        }
    }
    
    private final class TwoColumnTextNode extends Region {
        private Text measure = new Text("X");
        private Text left = new Text();
        private Text right = new Text();
        private double width;
        private double lineHeight = measure.getLayoutBounds().getHeight();
        
        public TwoColumnTextNode(double width){
            setWrappingWidth(width);
            left.setTextOrigin(VPos.TOP);
            right.setTextOrigin(VPos.TOP);
            setPrefWidth(width);
            getChildren().addAll(left,right);
            setMinHeight(USE_PREF_SIZE);
        }
        
        public void setWrappingWidth(double width) {
            this.width = width;
            setMinWidth(width);
            setMaxWidth(width);
            setPrefWidth(width);
            requestLayout();
        }
        
        public void setFont(Font font) {
            left.setFont(font);
            right.setFont(font);
            measure.setFont(font);
            lineHeight = measure.getLayoutBounds().getHeight();
        }
        
        public void setFill(Paint fill) {
            left.setFill(fill);
            right.setFill(fill);
        }
        
        public void setText(String text) {
            left.setText(text);
            right.setText(text);
        }
        
        @Override protected void layoutChildren() {
            final double width = getWidth();
            final double height = getHeight();
            final int colWidth = (int)((width-20)/2);
            left.setWrappingWidth(colWidth);
            right.setWrappingWidth(colWidth);
            right.setLayoutX(colWidth+20);
            
            int lines = (int)(height/lineHeight);
            if (lines < 9) lines = 9;
            double blockHeight = lines*lineHeight;
            
            right.setY(-blockHeight);
            Rectangle clip = new Rectangle(width,blockHeight);
            clip.setSmooth(false);
            setClip(clip);
        }

        @Override protected double computePrefHeight(double d) {
            final int colWidth = (int)((width-20)/2);
            left.setWrappingWidth(colWidth);
            int lines = (int)(left.getLayoutBounds().getHeight()/lineHeight);
            int halfLines = (int)Math.ceil(lines/2d);
            if (halfLines < 9) halfLines = 9;
            double blockHeight = Math.ceil(lines/2d)*lineHeight;
            return blockHeight;
        }
    }
}
